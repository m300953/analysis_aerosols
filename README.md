## Contact:
- Philipp Weiss, philipp.weiss@physics.ox.ac.uk
- Ross Herbert, ross.herbert@physics.ox.ac.uk
- Andrew Williams, andrew.williams@wolfson.ox.ac.uk 
- Philip Stier, philip.stier@physics.ox.ac.uk
- Climate Processes, Atmospheric, Oceanic and Planetary Physics, University of Oxford

## Simulations: 
- two simulations with and without aerosol perturbations over 10 days at 5 km resolution  
- prescribed SSTs and sea-ice from AMIP database (PCMDI-AMIP version 1.1.2), interpolated from monthly values
- spin-up of 10 days from 22 Aug 2020 to 01 Sep 2020
- simulation `ngc2009_irad32`:
    - location `/work/bm1235/b380952/experiments/ngc2009_OXF_10day_irad32/`
    - comments: no ocean model, land model JSBACH, no plume model
- simulation `ngc2009_irad33`:
    - location `/work/bm1235/b380952/experiments/ngc2009_OXF_10day_irad33/`
    - comments: no ocean model, land model JSBACH, plume model MACv2-SP with some modifications

## Plume model MACv2-SP: 
- original version described in publication from [Stevens et al.](https://gmd.copernicus.org/articles/10/433/2017/)
- coupling with graupel and radiation schemes:
    - dNovrN from plume model used to enhance background CDNC in graupel scheme (background CDNC of 80 cm-3)
    - dNovrN from plume model also used to enhance CDNC profile in radiation scheme (background CDNC of 80 cm-3 at surface decaying to 20 cm-3 at upper levels)
    - change in CDNC in graupel scheme solely impacts autoconversion rate 
 - relationship of AOD and CDNC updated from original to that used in [Herbert et al.](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2021JD034615)  
    - CDNC = a * ln(b * TAU + 1), where CDNC is in cm-3, TAU is column total AOD, a = 410 and b = 5 
- further modifications:
    - biomass burning plumes (S America, C S Africa, Maritime Continent) adjusted so that anthropogenic component is 75 percent of total AOD
    - seasonal cycle removed and replaced with constant weights taken from mean during September
    - annual weights extended beyond 2017 and held at 2017 values

![](./plots/simple_plumes.png)

## Output:
- grids:
    - on icon grid of 5 km resolution (R02B09) in folder `run_20200822T000000-20200831T235920`
    - on lon-lat grid of 1 degree resolution in folder `regrid_1deg`
    - on lon-lat grid of 0.1 degree resolution in folder `regrid_01deg`

- variables:   
    - file `*atm_2d_500_pl*.nc`, frequency 1h, air pressure 50000 Pa 
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | cli  | specific cloud ice content | kg kg-1 |
        | clw  | specific cloud ice content | kg kg-1 |
        | wa   | upward air velocity        | m s-1   |
        | rho  | air density                | kg m-3  |
    
        </sup></sub>
      
    - file `*atm_2d_cdnc_daily_ml*.nc`, frequency 1d
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | migCDNC  | CDNC following scaling by dNovrN in graupel scheme | cm-3 | 
    
        </sup></sub>
        
    - file `*atm_2d_land_ml*.nc`, frequency 1d
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | hydro_fract_snow_box    | fraction of snow on surface                | -                  |
        | hydro_w_snow_box        | water content of snow reservoir on surface | m water equivalent |
        | hydro_fract_water_box   | fraction of water on surface               | -                  |
        | hydro_w_skin_box        | water content in skin reservoir of surface | m water equivalent |
        | hydro_w_soil_column_box | water content in the whole soil column     | m water equivalent |
        | turb_rough_m_box        | surface roughness length for momentum      | m                  |
        | turb_rough_h_box        | surface roughness length for heat          | m                  |
        | seb_qsat_star_box       | surface specific humidity at saturation    | m2 s-2             |
    
        </sup></sub>
        
    - file `*atm_2d_rad*.nc`, frequency 1h
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | rsdt   | toa incident shortwave radiation                  | W m-2 |
        | rsut   | toa outgoing shortwave radiation                  | W m-2 |
        | rsutcs | toa outgoing clear-sky shortwave radiation        | W m-2 |
        | rlut   | toa outgoing longwave radiation                   | W m-2 |
        | rsds   | surface downwelling shortwave radiation           | W m-2 |
        | rlds   | surface downwelling longwave radiation            | W m-2 |
        | rsus   | surface upwelling shortwave radiation             | W m-2 |
        | rlus   | surface upwelling longwave radiation              | W m-2 |
        | rlutcs | toa outgoing clear-sky longwave radiation         | W m-2 |
        | rsdscs | surface downwelling clear-sky shortwave radiation | W m-2 |
        | rldscs | surface downwelling clear-sky longwave radiation  | W m-2 |
        | rsuscs | surface upwelling clear-sky shortwave radiation   | W m-2 |
    
        </sup></sub>

    - file `*atm_2d_std*.nc`, frequency 1h 
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | psl     | mean sea level pressure                 | Pa         |
        | ts      | surface temperature                     | K          |
        | clt     | total cloud cover                       | m2 m-2     |
        | prlr    | large-scale precipitation flux (water)  | kg m-2 s-1 |
        | prls    | large-scale precipitation flux (snow)   | kg m-2 s-1 |
        | pr      | precipitation flux                      | kg m-2 s-1 |
        | prw     | vertically integrated water vapour      | kg m-2     |
        | cllvi   | vertically integrated cloud water       | kg m-2     |
        | clivi   | vertically integrated cloud ice         | kg m-2     |
        | qgvi    | vertically integrated graupel           | kg m-2     |
        | qrvi    | vertically integrated rain              | kg m-2     |
        | qsvi    | vertically integrated snow              | kg m-2     |
        | cptgzvi | vertically integrated dry static energy | m2 s-2     |
        | hfls    | latent heat flux                        | W m-2      |
        | cptgzvi | vertically integrated dry static energy | m2 s-2     |
        | hfss    | sensible heat flux                      | W m-2      |
        | evspsbl | evaporation                             | kg m-2 s-1 |
        | tauu    | u-momentum flux at the surface          | N m-2      |
        | tauv    | v-momentum flux at the surface          | N m-2      |
        | sfcwind | 10m windspeed                           | m s-1      |
        | uas     | zonal wind in 10m                       | m s-1      |
        | vas     | meridional wind in 10m                  | m s-1      |
        | tas     | temperature in 2m                       | K          |
        | dew2    | dew point temperature in 2m             | K          | 
        | ptp     | tropopause air pressure                 | Pa         |
    
        </sup></sub>

    - file `*atm_2d_video*.nc`, frequency 15min 
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | prw   | vertically integrated water vapour | kg m-2 |
        | cllvi | vertically integrated cloud water  | kg m-2 |
        | clivi | vertically integrated cloud ice    | kg m-2 | 
    
        </sup></sub> 
        
    - file `*atm_3d_aero*.nc`, frequency 1d 
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | aer_aod_533 | aerosol optical depth at 533 nm            | - |
        | aer_ssa_533 | aerosol single scattering albedo at 533 nm | - | 
    
        </sup></sub>  
        
    - file `*atm_3d_cloud*.nc`, frequency 6h
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | clw | specific cloud water content | kg kg-1 |
        | cli | specific cloud ice content   | kg kg-1 | 
        | cl  | cloud area fraction          | m2 m-2  |
        | rho | air density                  | kg m-3  | 
    
        </sup></sub>   
  
    - file `*atm_3d_dyn*.nc`, frequency 6h
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | ua    | zonal wind        | m s-1 |
        | va    | meridional wind   | m s-1 | 
        | wa    | vertical velocity | m s-1 |
        | ta    | temperature       | K     | 
        | hus   | specific humidity | K     | 
        | pfull | air pressure      | Pa    |  
    
        </sup></sub>  
    
    - file `*atm_3d_radprof*.nc`, frequency 1d
        <sub><sup>
    
        | short name  | long name   | units       |
        | ----------- | ----------- | ----------- |
        | rsd    | downwelling shortwave radiation | W m-2 |
        | rsu    | upwelling shortwave radiation   | W m-2 | 
        | rld    | downwelling longwave radiation  | W m-2 |
        | rlu    | upwelling longwave radiation    | W m-2 | 
    
        </sup></sub>   
